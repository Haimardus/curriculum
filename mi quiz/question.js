function Quiz(questions) {
	this.score = 0;
	this.questions = questions;
	this.questionIndex = 0;
}

Quiz.prototype.getQuestionIndex = function() {
	return this.questions[this.questionIndex];
};

Quiz.prototype.guess = function(answer) {
	if (this.getQuestionIndex().isCorrectAnswer(answer)) {
		this.score++;
	}

	this.questionIndex++;
};

Quiz.prototype.isEnded = function() {
	return this.questionIndex === this.questions.length;
};

function Question(text, choices, answer) {
	this.text = text;
	this.choices = choices;
	this.answer = answer;
}

Question.prototype.isCorrectAnswer = function(choice) {
	return this.answer === choice;
};

function populate() {
	if (quiz.isEnded()) {
		showScores();
	} else {
		var element = document.getElementById('question');
		element.innerHTML = quiz.getQuestionIndex().text;

		var choices = quiz.getQuestionIndex().choices;
		for (var i = 0; i < choices.length; i++) {
			var element = document.getElementById('choice' + i);
			element.innerHTML = choices[i];
			guess('btn' + i, choices[i]);
		}

		showProgress();
	}
}

function guess(id, guess) {
	var button = document.getElementById(id);
	button.onclick = function() {
		quiz.guess(guess);
		populate();
	};
}

function showProgress() {
	var currentQuestionNumber = quiz.questionIndex + 1;
	var element = document.getElementById('progress');
	element.innerHTML = 'Question ' + currentQuestionNumber + ' of ' + quiz.questions.length;
}

function showScores() {
	var gameOverHTML = '<h1>Resulrados!</h1>';
	gameOverHTML += "<h2 id='score'> Tu resultado ha sido: " + quiz.score + '</h2>';
	var element = document.getElementById('quiz');
	element.innerHTML = gameOverHTML;
}

var questions = [
	new Question(
		'Cuantas líneas y espacios tiene un pentagrama?',
		[ '5 líneas y 4 espacios', '5 líneas y 5 espacios', '5 líneas y 6 espacios', '6 líneas y 5 espacios' ],
		'5 líneas y 4 espacios'
	),
	new Question(
		'¿Quién dirigió American Beauty?',
		[ 'Sam Mendes', 'Tom Hopper', 'Roman Polansky', 'Roman Polansky' ],
		'Sam Mendes'
	),
	new Question('A cuantas semicorcheas equivale una blanca?', [ '2', '4', '6', '8' ], '8'),
	new Question('¿Que cultura inventó el número cero?', [ 'Arabe', 'Griega', 'Egipcia', 'Romana' ], 'Arabe'),
	new Question(
		'¿En qué equipo jugó Carles Puyol durante su carrera como futbolista?',
		[ 'Barcelona', 'Milán', 'Juventus', 'Madrid' ],
		'Barcelona'
	),
	new Question(
		'¿Qué país ha ganado más veces el mundial de fútbol?',
		[ 'España', 'Brasil', 'Italia', 'Alemania' ],
		'Brasil'
	),
	new Question('¿En que año se produjo la revolución francesa?', [ '1776', '1492', '1789', '1881' ], '1789'),
	new Question(
		'¿En qué país se desarrolló la copa mundial de fútbol del año 2010?',
		[ 'Egipto', 'Sudáfrica', 'Brasil', 'Alemania' ],
		'Sudáfrica'
	),
	new Question('¿En que año se firmó en tratado de Versalles?', [ '1912', '1919', '1923', '1929' ], '1919'),
	new Question(
		'¿Cuál fue el primer James Bond del cine?',
		[ 'Sean Connery', 'David Niven', 'Barry Nelson', 'Andoni Garcia' ],
		'Sean Connery'
	),
	new Question(
		'Cual de estos no es un compositor Barroco?',
		[ 'Bach', 'Vivaldi', 'Pachelbel', 'Joseph Haydn' ],
		'Joseph Haydn'
	),
	new Question(
		'En Harry Potter ¿que hechizo produce un ataque de risa?',
		[ 'Accio', 'Rictusempra', 'Petrificus totalus', 'Lumos' ],
		'Rictusempra'
	)
];

var quiz = new Quiz(questions);

populate();
